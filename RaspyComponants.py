#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

# External libx
try:
	import RPi.GPIO as GPIO
except ModuleNotFoundError:
	print(
		"""Please, install the dependencies by running:
pip3 install -r requirements.txt"""
	)
# Modules
from src import ServoMotor

if __name__ == "__main__":
	servo = ServoMotor(pin=17)
	try:
		# servo.move_to(90)
		print(servo.increment_position(10))
	except KeyboardInterrupt:
		GPIO.cleanup()
		print("Bye.")
		exit(0)
	GPIO.cleanup()

