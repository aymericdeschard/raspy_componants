#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import time
# External libx
try:
	import RPi.GPIO as GPIO
except ModuleNotFoundError:
	print(
		"""Please, install the dependencies by running:
pip3 install -r requirements.txt"""
	)
	exit(1)
# Modules
from .Utils import transpose


class ServoMotor:
	"""Basic handler for a servo motor"""
	MIN_DUTY = 1.0 # ms
	MAX_DUTY = 12.5 # ms
	SERVO_RPM = 100

	def __init__(
		self,
		pin: int,
		min_angle: float = 0.0,
		max_angle: float = 180.0
	):
		"""
		:pin:		<Integer>	 The GPIO pin (BCM) of the signal for the servo
		:min_angle: <Float: 0>	 The minimum angle the servo can reach
		:max_angle: <Float: 180> The maximum angle the servo can reach
		"""
		self.pin = pin
		self.min_angle = min_angle
		self.max_angle = max_angle
		
		# Pin setup
		GPIO.setmode(GPIO.BCM)
		GPIO.setup(self.pin, GPIO.OUT)

		# servo init
		self.controller = GPIO.PWM(self.pin, 50) # Hz
		self.controller.start(2.5)
		self._clean_signal()

		self.position = None
	
	def move_to(self, angle: float) -> None:
		"""Move the shaft to a given angle and wait for it to have reach
		its destination

		:angle: <Float> The angle to move the shaft to, in degrees

		:returns: None
		"""
		# Calculate the time for the servo to reach its desired angle
		# calculate the degrees to travel
		timeToWait = abs((self.position or 0) - angle);
		# calculate the time in seconds to do the journey
		# 1 RPM = 6 degree/second
		timeToWait = timeToWait / (self.SERVO_RPM * 6);

		signal = transpose(
			angle,
			self.min_angle,
			self.max_angle,
			self.MIN_DUTY,
			self.MAX_DUTY
		)
		self.controller.ChangeDutyCycle(signal)
		time.sleep(timeToWait)
		self.position = angle
		self._clean_signal()

	def increment_position(self, value: int) -> int:
		"""Increment or decrement the shaft position by x degrees.
		
		Raise a ValueError if the increment will overshoot the shaft 
		maximum or minimum positon.

		:value: <Integer>	The number of degrees to increment (if positive)
							or decrement (if negative) the current position.
		:returns: the updated position

		"""
		if not self.position:
			raise ValueError("Cannot increment position as the current position is unknown.")
		elif self.position + value < self.min_angle \
			 or self.position + value > self.max_angle:
			raise ValueError(
				f"Cannot increment position by {value} \
as the target value {self.position + value} \
will overshoot the minimum {self.min_angle} \
or maximum {self.max_angle} of the shaft."
			)

		self.move_to(self.position + value)
		return self.position

	def test(self) -> None:
		"""Test the good condition of the servo by moving the shaft at
		different angles indefinitly.
		For testing the health of the servo or the wiring.

		:returns: None
		"""
		while True:
			for i in range(0, 181, 45):
				self.move_to(i)
				print(f"Move to {i}")
				time.sleep(0.5)

	def _clean_signal(self) -> None:
		"""Set the pinout signal to zero to avoid jiggling of the shaft.
		
		:returns: None

		"""
		self.controller.ChangeDutyCycle(0)

	def __del__(self):
		try:
			self.controller.stop()
		except AttributeError:
			pass
