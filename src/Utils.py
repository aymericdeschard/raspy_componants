#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

def transpose(
	value: float,
	in_min: float,
	in_max: float,
	out_min: float,
	out_max: float
) -> float:
	"""Transpose a value of a given range to its equivalent to another range.

	:value: <Float> The value to transpose
	:in_min: <Float> The minimum of the origin range
	:in_max: <Float> The maximum of the origin range
	:out_min: <Float> The minimum of the target range
	:out_max: <Float> The maximum of the target range

	:returns: <Float> The transposed value
	"""
	return (value - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

